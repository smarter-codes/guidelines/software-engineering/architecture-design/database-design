# Database Design

## Plan
* Pick the Right model: [Graph vs Document vs Relational](http://www.linkeddatatools.com/introducing-rdf) vs Time-Series
* Pick the location: [Client side](https://web.dev/storage-for-the-web/) or Server side? [or both?](https://www.apollographql.com/#data)

## Document
* ORM in Code?
* Database Diagram?

## Tune
* Optimizing for Response time
* Optimizing for Size
* Optimizing for Fault tolerance
